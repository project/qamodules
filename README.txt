Quick Admin Modules
------------------------
Add client-side dependency checking to Modules Administration page.

If you have ever been frustrated having to submit the Modules page multiple
times to turn off a set of nested modules, this module is for you. When you
click a checkbox to turn on a module, it also turns on all the modules it
depends on. And when you turn off a module, it turns off all of the modules
that depend on it.

Installation:
-------------------------
Simply go to the module administration page and enable the module and
click Save Configuration.
